# TP1 - systemd

- [TP1 - systemd](#tp1---systemd)
- [Intro](#intro)
  - [Objectifs du TP](#objectifs-du-tp)
  - [Prerequisites](#prerequisites)
- [0. Préparation de la machine](#0-pr%c3%a9paration-de-la-machine)
- [I. `systemd`-basics](#i-systemd-basics)
  - [1. First steps](#1-first-steps)
  - [2. Gestion du temps](#2-gestion-du-temps)
  - [3. Gestion de noms](#3-gestion-de-noms)
  - [4. Gestion du réseau (et résolution de noms)](#4-gestion-du-r%c3%a9seau-et-r%c3%a9solution-de-noms)
    - [NetworkManager](#networkmanager)
    - [`systemd-networkd`](#systemd-networkd)
    - [`systemd-resolved`](#systemd-resolved)
  - [5. Gestion de sessions `logind`](#5-gestion-de-sessions-logind)
  - [6. Gestion d'unité basique (services)](#6-gestion-dunit%c3%a9-basique-services)
- [II. Boot et Logs](#ii-boot-et-logs)
- [III. Mécanismes manipulés par systemd](#iii-m%c3%a9canismes-manipul%c3%a9s-par-systemd)
  - [1. cgroups](#1-cgroups)
  - [2. D-Bus](#2-d-bus)
  - [3. Restriction et isolation](#3-restriction-et-isolation)
- [IV. systemd units in-depth](#iv-systemd-units-in-depth)
  - [1. Exploration de services existants](#1-exploration-de-services-existants)
  - [2. Création de service simple](#2-cr%c3%a9ation-de-service-simple)
  - [3. Sandboxing (heavy security)](#3-sandboxing-heavy-security)
  - [4. Event-based activation](#4-event-based-activation)
    - [A. Activation via socket UNIX](#a-activation-via-socket-unix)
    - [B. Activation automatique d'un point de montage](#b-activation-automatique-dun-point-de-montage)
    - [C. Timer `systemd`](#c-timer-systemd)
  - [5. Ordonner et grouper des services](#5-ordonner-et-grouper-des-services)
- [Conclusion](#conclusion)

# 0. Préparation de la machine

```
$ sudo yum install -y vim bind-utils tcpdump dbus-tools systemd-container
$ sudo setenforce 0
$ sudo sed -i '7s/enforcing/permissive/' /etc/selinux/config
```

# I. `systemd`-basics

## 1. First steps

systemd est en version supérieure à 241 :

```
$ systemctl --version
systemd 243 (v243.4-1.fc31)
```

---

🌞 s'assurer que `systemd` est PID1

```
$ ps -eo pid,cmd | grep -E '^ *1 .*$'
      1 /usr/lib/systemd/systemd --switched-root --system --deserialize 30
```

---

🌞 check tous les autres processus système (**PAS les processus kernel)
```
$ ps -eo pid,cmd | grep -v -E '^ *[0-9]* \[.*\]$'
    607 /sbin/auditd
    626 /usr/bin/python3 /usr/sbin/firewalld --nofork --nopid
    627 /usr/sbin/sssd -i --logger=files
    629 /usr/sbin/chronyd
    638 dbus-broker --log 4 --controller 9 --machine-id 225a061d4d4b4e70b7dce462fa4bdd57 --max-bytes 536870912 --max-fds 4096 --max-matches 131072 --audit
```
* `auditd` : démon d'audit sur des machines GNU/Linux. C'est par exemple lui qui est chargé d'enregistrer les logs de SELinux. On peut les trouver dans `/var/log/audit/audit.log`
* `firewalld` : c'est un outil permettant de mettre en place du firewaling  sur des OS basés sur RedHat. C'est une surcouche à `iptables` ou `nftables`
* `sssd` démon d'authentification et d'identification des systèmes GNU/Linux. Permet d'être client d'un annuaire LDAP, d'un Active Directory ou encore d'un démon Kerberos
* `chronyd` : démon NTP (synchronisation de l'heure)
* `dbus-broker` : démon D-Bus maintenant en vie un bus D-Bus. [Implémentation moderne développée à des fins de robustesse](https://lwn.net/Articles/731755/).

Il y a très peu de services qui tournent au démarrage de l'OS : 
```
$ ps -eo pid,cmd | grep -v -E '^ *[0-9]* \[.*\]$'
    PID CMD
      1 /usr/lib/systemd/systemd --switched-root --system --deserialize 30
    530 /usr/lib/systemd/systemd-journald
    544 /usr/lib/systemd/systemd-udevd
    607 /sbin/auditd
    626 /usr/bin/python3 /usr/sbin/firewalld --nofork --nopid
    627 /usr/sbin/sssd -i --logger=files
    629 /usr/sbin/chronyd
    637 /usr/bin/dbus-broker-launch --scope system --audit
    638 dbus-broker --log 4 --controller 9 --machine-id 225a061d4d4b4e70b7dce462fa4bdd57 --max-bytes 536870912 --max-fds 4096 --max-matches 131072 --audit
    639 /usr/libexec/sssd/sssd_be --domain implicit_files --uid 0 --gid 0 --logger=files
    640 /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=files
    642 /usr/sbin/NetworkManager --no-daemon
    648 /usr/lib/systemd/systemd-logind
    655 /usr/sbin/sshd -D -oCiphers=<LONG_LIST>
    667 login -- it4
    753 /usr/lib/systemd/systemd --user
    755 (sd-pam)
    760 -bash
    821 sshd: it4 [priv]
    824 sshd: it4@pts/0
    825 -bash
   9626 ps -eo pid,cmd
   9627 grep --color=auto -v -E ^ *[0-9]* \[.*\]$
```

## 2. Gestion du temps 

🌞 déterminer la différence entre Local Time, Universal Time et RTC time
* le RTC time est le temps de la pile hardware
* l'Universal Time est le temps UTC récupéré par le démon NTP
* le Local Time est le temps déduit de l'UTC après application du fuseau horaire

Note : la pile hardware est elle aussi resynchronisée par le démon NTP (depuis l'UTC donc).

Le RTC est particulièrement utilisé dans les systèmes embarqués car beaucoup plus fiable qu'une pile software.

---


* timezones
```
$ timedatectl list-timezones | grep Europe
$ sudo timedatectl set-timezone Europe/Madrid
```

---

* on peut activer ou désactiver l'utilisation de la syncrhonisation NTP avec `timedatectl set-ntp <BOOLEAN>`
  * 🌞 désactiver le service lié à la synchronisation du temps avec cette commande, et vérifier à la main qu'il a été coupé
```
$ sudo systemctl is-active chronyd
active
$ sudo timedatectl set-ntp false
$ sudo systemctl is-active chronyd
inactive
```

## 3. Gestion de noms

🌞 expliquer la différence entre les trois types de noms. Lequel est à utiliser pour des machines de prod ?
* `static` est le vrai nom d'host de la machine, utilisé sur le réseau. Pour de la prod on définit celui-ci
* `pretty` est le p'tit nom mignon donné à la machine. Typiquement utilisé par des utilisateurs finaux avec "Ordinateur de Super'Léo" (majuscules, caractères sépciaux, etc.)
* `transient` : utilisé par le kernel pour certaines opérations. Ce n'est qu'un état transitoire qu'il n'est pas recommandé d'utiliser directement.

## 4. Gestion du réseau (et résolution de noms)

### NetworkManager

🌞 afficher les informations DHCP récupérées par NetworkManager (sur une interface en DHCP)
```
$ sudo nmcli con show enp0s3 help | grep -i DHCP4
DHCP4.OPTION[1]:                        dhcp_lease_time = 86400
DHCP4.OPTION[2]:                        dhcp_rebinding_time = 75600
DHCP4.OPTION[3]:                        dhcp_renewal_time = 43200
DHCP4.OPTION[4]:                        dhcp_server_identifier = 10.0.2.2
DHCP4.OPTION[5]:                        domain_name = home
DHCP4.OPTION[6]:                        domain_name_servers = 192.168.1.1
DHCP4.OPTION[7]:                        expiry = 1577098115
DHCP4.OPTION[8]:                        ip_address = 10.0.2.15
DHCP4.OPTION[9]:                        requested_broadcast_address = 1
DHCP4.OPTION[10]:                       requested_dhcp_server_identifier = 1
DHCP4.OPTION[11]:                       requested_domain_name = 1
DHCP4.OPTION[12]:                       requested_domain_name_servers = 1
DHCP4.OPTION[13]:                       requested_domain_search = 1
DHCP4.OPTION[14]:                       requested_host_name = 1
DHCP4.OPTION[15]:                       requested_interface_mtu = 1
DHCP4.OPTION[16]:                       requested_ms_classless_static_routes = 1
DHCP4.OPTION[17]:                       requested_nis_domain = 1
DHCP4.OPTION[18]:                       requested_nis_servers = 1
DHCP4.OPTION[19]:                       requested_ntp_servers = 1
DHCP4.OPTION[20]:                       requested_rfc3442_classless_static_routes = 1
DHCP4.OPTION[21]:                       requested_root_path = 1
DHCP4.OPTION[22]:                       requested_routers = 1
DHCP4.OPTION[23]:                       requested_static_routes = 1
DHCP4.OPTION[24]:                       requested_subnet_mask = 1
DHCP4.OPTION[25]:                       requested_time_offset = 1
DHCP4.OPTION[26]:                       requested_wpad = 1
DHCP4.OPTION[27]:                       routers = 10.0.2.2
DHCP4.OPTION[28]:                       subnet_mask = 255.255.255.0
```

### `systemd-networkd`

🌞 stopper et désactiver le démarrage de `NetworkManager`
🌞 démarrer et activer le démarrage de `systemd-networkd`
```
sudo systemctl disable --now NetworkManager
sudo systemctl enable --now systemd-networkd
```

---

🌞 éditer la configuration d'une carte réseau de la VM avec un fichier `.network`
```
$ cat /etc/systemd/network/enp0sX.network 
[Match]
Name=enp0s*

[Network]
DHCP=ipv4

$ sudo systemctl restart systemd-networkd
```

### `systemd-resolved`

🌞 activer la résolution de noms par `systemd-resolved` en démarrant le service (maintenant et au boot)
🌞 vérifier qu'un serveur DNS tourne localement et écoute sur un port de l'interfce localhost (avec `ss` par exemple)

```
$ sudo systemctl enable --now systemd-resolved
$ sudo ss -alntp | grep 53
LISTEN    0         128          127.0.0.53%lo:53               0.0.0.0:*        users:(("systemd-resolve",pid=10062,fd=19))                                    
LISTEN    0         128                0.0.0.0:5355             0.0.0.0:*        users:(("systemd-resolve",pid=10062,fd=13))                                    
LISTEN    0         128                   [::]:5355                [::]:*        users:(("systemd-resolve",pid=10062,fd=15)) 

$ dig l42.fr @127.0.0.53
[...]
;; ANSWER SECTION:
l42.fr.			86383	IN	A	62.210.5.33
[...]
;; SERVER: 127.0.0.53#53(127.0.0.53)

$ systemd-resolve l42.fr
l42.fr: 62.210.5.33                            -- link: enp0s3

-- Information acquired via protocol DNS in 4.8ms.
-- Data is authenticated: no
```

---

🌞 Afin d'activer de façon permanente ce serveur DNS, la bonne pratique est de remplacer `/etc/resolv.conf` par un lien symbolique pointant vers `/run/systemd/resolve/stub-resolv.conf`
🌞 Modifier la configuration de `systemd-resolved`
```
$ sudo mv /etc/resolv.conf /etc/resolv.conf.old
$ sudo ln -s /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf

$ resolvectl | grep "Current DNS"
  Current DNS Server: 192.168.1.1
$ sudo vim /etc/systemd/resolved.conf
$ cat /etc/systemd/resolved.conf
[...]
[Resolve]
DNS=1.1.1.1
[...]

$ sudo systemctl restart systemd-resolved
$ resolvectl | grep "Current DNS"
  Current DNS Server: 1.1.1.1
  Current DNS Server: 192.168.1.1 # DHCP-received address
```

---

🌞 mise en place de DNS over TLS

```
$ cat /etc/systemd/resolved.conf  | grep TLS
DNSOverTLS=yes

$ sudo tcpdump -n -nn -i enp0s3 
[...]
12:50:36.743786 IP 10.0.2.15.40872 > 1.1.1.1.853: Flags [P.], seq 1348:1432, ack 2270, win 64011, length 84
12:50:36.744313 IP 1.1.1.1.853 > 10.0.2.15.40872: Flags [.], ack 1432, win 65535, length 0
12:50:36.760007 IP 1.1.1.1.853 > 10.0.2.15.40872: Flags [P.], seq 2270:2668, ack 1432, win 65535, length 398
[...]

# Sur un autre terminal
$ systemd-resolve bob.com
```

Le port 853 est bien utilisé sur le DNS de CloudFlare (1.1.1.1:853). En exportant dans un wireshark la capture, les données sont bien chiffrées (tunnel TLS)

---

🌞 activer l'utilisation de DNSSEC

```
$ cat /etc/systemd/resolved.conf  | grep DNSSEC
DNSSEC=yes

$ resolvectl query dnssec-or-not.com
dnssec-or-not.com: 173.230.152.222             -- link: enp0s3

-- Information acquired via protocol DNS in 6.8ms.
-- Data is authenticated: yes 

# "Data is authenticated :)"

$ dig +dnssec dnssec-or-not.com

;; flags: qr rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1 # AD flag is DNSSEC-related :)

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 65494
; OPT=5: 05 07 08 0a 0d 0e 0f (".......") # These options are DNSSEC-related too
; OPT=6: 01 02 04 ("...") # here
; OPT=7: 01 (".")  # and here
;; QUESTION SECTION:
;dnssec-or-not.com.		IN	A

;; ANSWER SECTION:
dnssec-or-not.com.	3561	IN	A	173.230.152.222

;; Query time: 1 msec
;; SERVER: 127.0.0.53#53(127.0.0.53) # it's our local server
;; WHEN: Sun Dec 08 12:59:19 CET 2019
;; MSG SIZE  rcvd: 85
```

## 6. Gestion d'unité basique (services)

🌞 trouver l'unité associée au processus `chronyd`
```
$ sudo timedatectl set-ntp true
$ ps -ef | grep chronyd
chrony     10887       1  0 13:01 ?        00:00:00 /usr/sbin/chronyd

$ systemctl status 10887 | head -n 1
● chronyd.service - NTP client/server
```

# II. Boot et Logs

* 🌞 générer un graphe de la séquence de boot
```
$ sudo systemd-analyze plot > graphe.svg`
```

[Le graphe est dans le répertoire files/ .](./files/graphe.svg)

---

# III. Mécanismes manipulés par systemd

## 1. cgroups

* 🌞 identifier le cgroup utilisé par votre session SSH
  * identifier la RAM maximale à votre disposition (dans `/sys/fs/cgroup`)

```
$ ps -eo cmd,cgroup | grep ssh
/usr/sbin/sshd -D -oCiphers 0::/system.slice/sshd.service
sshd: it4 [priv]            0::/user.slice/user-1000.slice/session-4.scope
sshd: it4@pts/0             0::/user.slice/user-1000.slice/session-4.scope
sshd: it4 [priv]            0::/user.slice/user-1000.slice/session-5.scope
sshd: it4@pts/1             0::/user.slice/user-1000.slice/session-5.scope

$ cat /sys/fs/cgroup/user.slice/user-1000.slice/session-4.scope/memory.max 
max

# la session ssh actuelle a accès au max de la RAM dispo, soit...

$ free -hm
              total        used        free      shared  buff/cache   available
Mem:          1.9Gi       161Mi       1.3Gi       0.0Ki       487Mi       1.6Gi
Swap:         819Mi          0B       819Mi
```

---

* 🌞 modifier la RAM dédiée à votre session utilisateur
  * `systemctl set-property <SLICE_NAME> MemoryMax=512M`
  * vérifier le changement
    * toujours dans `/sys/fs/cgroup`
* la commande `systemctl set-property` génère des fichiers dans `/etc/systemd/system.control/`
  * 🌞 vérifier la création du fichier
  * on peut supprimer ces fichiers pour annuler les changements

```
$ cat /sys/fs/cgroup/user.slice/user-1000.slice/memory.max
max
$ sudo systemctl set-property user-1000.slice MemoryMax=512M
$ cat /sys/fs/cgroup/user.slice/user-1000.slice/memory.max
536870912

$ ls -al /etc/systemd/system.control/user-1000.slice.d/50-MemoryMax.conf 
-rw-r--r--. 1 root root 149 Dec 08 13:09 /etc/systemd/system.control/user-1000.slice.d/50-MemoryMax.conf
$ cat /etc/systemd/system.control/user-1000.slice.d/50-MemoryMax.conf 
# This is a drop-in unit file extension, created via "systemctl set-property"
# or an equivalent operation. Do not edit.
[Slice]
MemoryMax=536870912
```

## 2. D-Bus

🌞 observer, identifier, et expliquer complètement un évènement choisi

---

🌞 envoyer un signal pour générer un évènement

On va appeler une méthode et récupérer une propriété. En partant de rien. La commande `busctl` est notre meilleure amie pour explorer l'état des bus D-Bus depuis la ligne de commande.

```
# Liste de tous les objets D-Bus actuellement registered
$ busctl list

# Obtention du path d'un objet en ayant son nom
$ busctl tree org.freedesktop.hostname1

# Récupération des méthodes, signaux et propriétés exposées par l'objet
$ busctl introspect org.freedesktop.hostname1 /org/freedesktop/hostname1

# Récupération d'une propriété 
$ busctl get-property org.freedesktop.hostname1 /org/freedesktop/hostname1 org.freedesktop.hostname1 KernelRelease

# Appel d'une méthode
$ busctl call org.freedesktop.hostname1 /org/freedesktop/hostname1 org.freedesktop.DBus.Peer GetMachineId
s "225a061d4d4b4e70b7dce462fa4bdd57"
```

## 3. Restriction et isolation

Lancer un processus sandboxé, et tracé, avec `systemd-run`

---

🌞 identifier le cgroup utilisé

```
$ sudo systemd-run --wait -t /bin/bash
Running as unit: run-u383.service
Press ^] three times within 1s to disconnect TTY.
[root@localhost /]# 

# Dans un autre terminal
$ systemctl status run-u383.service | grep CGroup
   CGroup: /system.slice/run-u383.service

# on peut grep 'root' car le systemd-run a été lancé en sudo
$ ps -eo pid,user,cmd,cgroup | grep bash | grep root
  11661 root     /bin/bash                   0::/system.slice/run-u383.service

$ ls /sys/fs/cgroup/system.slice/run-u383.service/
```

---

🌞 ajouter des restrictions cgroups

Sans restriction :
```
$ sudo systemd-run --wait -t /bin/bash
Running as unit: run-u391.service
Press ^] three times within 1s to disconnect TTY.
[root@localhost /]# 

# Autre terminal
$ cat /sys/fs/cgroup/system.slice/run-u391.service/memory.max 
max
```

Avec restriction :
```
$ sudo systemd-run -p MemoryMax=512M --wait -t /bin/bash
Running as unit: run-u394.service
Press ^] three times within 1s to disconnect TTY.
[root@localhost /]# 

# Autre terminal
$ cat /sys/fs/cgroup/system.slice/run-u394.service/memory.max 
536870912
```

---

🌞 ajouter un traçage réseau

```
# With NO traffic

$ sudo systemd-run -p IPAccounting=true --wait -t /bin/bash
Running as unit: run-u397.service
Press ^] three times within 1s to disconnect TTY.

[root@localhost /]# exit
Finished with result: success
Main processes terminated with: code=exited/status=0
Service runtime: 2.136s
CPU time consumed: 30ms
IP traffic received: 0B
IP traffic sent: 0B


# With traffic

$ sudo systemd-run -p IPAccounting=true --wait -t /bin/bash
Running as unit: run-u400.service
Press ^] three times within 1s to disconnect TTY.

[root@localhost /]# ping 8.8.8.8 -c 4
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=16.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=16.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=63 time=17.6 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=63 time=18.4 ms
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 16.915/17.447/18.384/0.597 ms

[root@localhost /]# exit
Finished with result: success
Main processes terminated with: code=exited/status=0
Service runtime: 7.003s
CPU time consumed: 45ms
IP traffic received: 336B
IP traffic sent: 336B
```

---

🌞 ajouter des restrictions réseau

Sans restriction :
```
$ sudo systemd-run --wait -t /bin/bash
Running as unit: run-u406.service
Press ^] three times within 1s to disconnect TTY.
[root@localhost /]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=17.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=17.1 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 17.093/17.152/17.212/0.059 ms
[root@localhost /]# ping 10.1.1.1
PING 10.1.1.1 (10.1.1.1) 56(84) bytes of data.
64 bytes from 10.1.1.1: icmp_seq=1 ttl=64 time=0.501 ms
64 bytes from 10.1.1.1: icmp_seq=2 ttl=64 time=0.434 ms
^C
--- 10.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.434/0.467/0.501/0.033 ms
```

Avec restriction :
```
$ sudo systemd-run -p IPAddressAllow=10.1.1.0/24 -p IPAddressDeny=any --wait -t /bin/bash
Running as unit: run-u411.service
Press ^] three times within 1s to disconnect TTY.
[root@localhost /]# ping 8.8.8.8 -c 3
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
ping: sendmsg: Operation not permitted
ping: sendmsg: Operation not permitted
ping: sendmsg: Operation not permitted
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 2028ms

[root@localhost /]# ping 10.1.1.1 -c 2
PING 10.1.1.1 (10.1.1.1) 56(84) bytes of data.
64 bytes from 10.1.1.1: icmp_seq=1 ttl=64 time=0.492 ms
64 bytes from 10.1.1.1: icmp_seq=2 ttl=64 time=0.350 ms
--- 10.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.350/0.421/0.492/0.071 ms
```

---

Lancer un processus complètement sandboxé (conteneur ?) avec `systemd-nspawn`.

`systemd-nspawn` permet de créer un simili-container géré par systemd. Il est capable d'appliquer des restrictions namespaces et cgroups (entre autres) et d'y exécuter un ou plusieurs processus. 

`--ephemeral` permet de détruire l'environnement ainsi créé à la fin de l'exécution du processus.

`--private-network` permet d'isoler le processus dans un nouveau namespace réseau.

```
$ sudo systemd-nspawn -D / -x --private-network -U /bin/bash
Spawning container nowhere.localhost-1b82d2d0f73b4ccb on /.#machine.6c402b91e01b8436.
Press ^] three times within 1s to kill container.
Selected user namespace base 97910784 and range 65536.
[root@nowhere /]# 

# Notice how the root user is used (inside the container)

# From another terminal
$ ps -ef | grep bash
root        6281    6280  0 14:26 pts/3    00:00:00 systemd-nspawn -D / -x --private-network -U /bin/bash
97910784    6284    6281 10 14:26 pts/0    00:00:00 /bin/bash

# Let's see our own namespace
$ ls -al /proc/$$/ns
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:28 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:28 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:28 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:26 net -> 'net:[4026532008]'
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:28 pid -> 'pid:[4026531836]'
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:28 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:28 user -> 'user:[4026531837]'
lrwxrwxrwx 1 it4 it4 0 Dec 08 14:28 uts -> 'uts:[4026531838]'

# Container namespaces
$ sudo ls -aln /proc/6284/ns
dr-x--x--x 2 97910784 97910784 0 Dec 08 14:29 .
dr-xr-xr-x 9 97910784 97910784 0 Dec 08 14:26 ..
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 cgroup -> 'cgroup:[4026532765]'
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 ipc -> 'ipc:[4026532539]'
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 mnt -> 'mnt:[4026532537]'
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 net -> 'net:[4026532541]'
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 pid -> 'pid:[4026532540]'
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 pid_for_children -> 'pid:[4026532540]'
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 user -> 'user:[4026532536]'
lrwxrwxrwx 1 97910784 97910784 0 Dec 08 14:29 uts -> 'uts:[4026532538]'
```

Les seuls namespaces communs sont IPC et UTS. 

L'option `-U` a permis d'isoler le processus dans un nouveau namespace user.

# IV. systemd units in-depth

🌞 observer l'unité `auditd.service`
* l'unité de service `auditd` est définie dans `/usr/lib/systemd/system/auditd.service` :
```
$ systemctl status auditd
● auditd.service - Security Auditing Service
   Loaded: loaded (/usr/lib/systemd/system/auditd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:auditd(8)
           https://github.com/linux-audit/audit-documentation
```

## 2. Création de service simple

🌞 Créer un fichier dans `/etc/systemd/system` qui comporte le suffixe `.service` :

```
$ sudo vim /etc/systemd/system/web.service
$ sudo systemctl daemon-reload

$ systemctl cat web
# /etc/systemd/system/web.service
[Unit]
Description=Web dummy service
After=network.target

[Service]
Type=simple
KillMode=process

ExecStartPre=/usr/bin/firewall-cmd --add-port=8888/tcp
ExecStartPre=/usr/bin/firewall-cmd --add-port=8888/tcp --permanent
ExecStart=/usr/bin/python3 -m http.server 8888
ExecStartPost=/usr/bin/firewall-cmd --remove-port=8888/tcp
ExecStartPost=/usr/bin/firewall-cmd --remove-port=8888/tcp --permanent

MemoryMax=256M

[Install]
WantedBy=multi-user.target

$ sudo systemctl start web
```

---

🌞 faites en sorte que le service se lance au démarrage

La commande `systemctl enable` ajoute un lien symbolique du fichier de service dans un dossier de drop-in lié à un target systemd.  
Plus précisément, le target est celui précisé dans la section `[Install]` de l'unité de service.  

Ainsi, indiquer `WantedBy=multi-user.target` aura pour effet de créer un lien symbolique dans `/etc/systemd/system/multi-user.target.wants/web.service`. Une fois en place, l'unité de service en question sera démarré en même temps que le target lié.  

Ici, `web.service` sera démarré par `multi-user.target`.

```
$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

## 3. Sandboxing (heavy security)

Essayez d'obtenir le meilleur score avec `systemd-analyze security <SERVICE>`, en comprenant ce que vous faites.

Avant :
```
$ systemd-analyze security web
→ Overall exposure level for web.service: 9.6 UNSAFE 😨
```

Après :
```
$ systemd-analyze security web
→ Overall exposure level for web.service: 4.4 OK 🙂
```

```
$ sudo systemctl cat web
# /etc/systemd/system/web.service
[Unit]
Description=Web dummy service
After=network.target

[Service]
Type=simple
KillMode=process

ExecStartPre=/usr/bin/firewall-cmd --add-port=8888/tcp
ExecStartPre=/usr/bin/firewall-cmd --add-port=8888/tcp --permanent
ExecStart=/usr/bin/python3 -m http.server 8888
ExecStopPost=/usr/bin/firewall-cmd --remove-port=8888/tcp
ExecStopPost=/usr/bin/firewall-cmd --remove-port=8888/tcp --permanent

# CGroup restrictions
MemoryMax=256M

# Isolation
RestrictNamespaces=true # empêche la création de nouveau namespaces

## Network
IPAddressDeny=any # empêche de joindre le réseau
IPAddressAllow=10.1.1.0/24 # whitelist un réseau spécifique

## FS
PrivateDevices=true # monte un répertoire /dev dédié au processus
PrivateTmp=true # monte un répertoire /tmp dédié au processus (sous-répertoire dans /tmp)
ProtectHome=true
ProtectSystem=true
ProtectControlGroups=true
ProtectKernelModules=true
ProtectKernelTunables=true

# Syscalls filters
SystemCallArchitectures=native

# Capabilities
CapabilityBoundingSet=CAP_NET_ADMIN # définit un nouveau jeu de capa

[Install]
WantedBy=multi-user.target
```

## 4. Event-based activation 

🌞 Faire en sorte que Docker démarre tout seul s'il est sollicité

Setup :
```
$ sudo systemctl cat docker.socket
# /etc/systemd/system/docker.socket
[Unit]
Description=Docker Socket for the API
PartOf=docker.service

[Socket]
ListenStream=/var/run/docker.sock
SocketMode=0660
SocketUser=root
SocketGroup=docker

[Install]
WantedBy=sockets.target
```

```
$ sudo systemctl enable --now docker.socket
```

Test : 
```
$ sudo systemctl is-active docker.socket
active
$ sudo systemctl is-active docker.service
inactive

$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
$ sudo systemctl is-active docker.socket
active
$ sudo systemctl is-active docker.service
active
```

### B. Activation automatique d'un point de montage

Setup :
```
$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part 
  ├─fedora-root 253:0    0  6.2G  0 lvm  /
  └─fedora-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0    8G  0 disk 
└─sdb1            8:17   0    8G  0 part 
```

```
$ sudo systemctl cat opt-yo.mount
# /etc/systemd/system/opt-yo.mount
[Unit]
Description=Mount /opt/yo

[Mount]
What=/dev/sdb1
Where=/opt/yo
Type=ext4
Options=defaults

[Install]
WantedBy=multi-user.target
```

```
$ sudo systemctl cat opt-yo.automount
# /etc/systemd/system/opt-yo.automount
[Unit]
Description=Automount /opt/yo
ConditionPathExists=/opt/yo

[Automount]
Where=/opt/yo
TimeoutIdleSec=10

[Install]
WantedBy=multi-user.target
```

```
$ sudo systemctl enable --now opt-yo.automount
```

Test :
```
$ date  
Sun 8 Dec 2019 03:33:19 PM CET

$ df -h | grep opt
# nothing

$ systemctl is-active opt-yo.mount
inactive

$ ls -al /opt/yo/
drwxr-xr-x. 3 root root  4096 Dec 08 15:21 .
drwxr-xr-x. 4 root root    34 Dec 08 15:22 ..
drwx------. 2 root root 16384 Dec 08 15:21 lost+found

$ df -h | grep opt
/dev/sdb1                7.9G   36M  7.4G   1% /opt/yo
$ systemctl is-active opt-yo.mount
active

# wait a bit
$ date
Sun 8 Dec 2019 03:35:29 PM CET

$ df -h | grep opt
# nothing

$ systemctl is-active opt-yo.mount
inactive
```

### C. Timer `systemd`

Setup :
```
$ cat /opt/backup.sh 
#!/bin/bash

cp -R /opt/yo/* /data

$ systemctl cat backup.service
# /etc/systemd/system/backup.service
[Unit]
Description=Backup service test
Requires=tmp-yo.automount

[Service]
Type=oneshot
ExecStart=/opt/backup.sh

[Install]
WantedBy=multi-user.target

$ systemctl cat backup.timer
# /etc/systemd/system/backup.timer
[Unit]
Description=Start backup.sh daily

[Timer]
#OnCalendar=*-*-* 04:00:00
OnCalendar=minutely

[Install]
WantedBy=timers.target

```

Test :
```
$ ls /opt/yo
lost+found
$ ls /data/
lost+found
$ sudo touch /opt/yo/test

# Wait a bit

$ ls /opt/yo
lost+found  test
$ ls /data/
lost+found

$ sudo systemctl list-timers --no-pager
NEXT                         LEFT       LAST                         PASSED      UNIT                         ACTIVATES
Sun 2019-12-08 15:50:00 CET  56s left   Sun 2019-12-08 15:49:03 CET  55ms ago    backup.timer                 backup.service
[...]
4 timers listed.

$ ls /data/
lost+found  test
```

## 5. Ordonner et grouper des services


🌞 créer un `target` systemd (inspirez-vous des `target` existants, et de la doc)

Setup :
```
$ systemctl cat web.target
# /etc/systemd/system/web.target
[Unit]
Description=Webapp
Requires=multi-user.target
After=multi-user.target

[Install]
WantedBy=multi-user.target
```

```
$ systemctl cat web.service
# /etc/systemd/system/web.service
[Unit]
Description=Web dummy service
After=tmp-yo.automount
PartOf=web.target

[Service]
Type=simple
KillMode=process

ExecStartPre=/usr/bin/firewall-cmd --add-port=8888/tcp
ExecStartPre=/usr/bin/firewall-cmd --add-port=8888/tcp --permanent
ExecStart=/usr/bin/python3 -m http.server 8888
ExecStopPost=/usr/bin/firewall-cmd --remove-port=8888/tcp
ExecStopPost=/usr/bin/firewall-cmd --remove-port=8888/tcp --permanent

WorkingDirectory=/opt/yo

[Install]
WantedBy=web.target
```

```
$ systemctl cat opt-yo.mount 
# /etc/systemd/system/opt-yo.mount
[Unit]
Description=Mount /opt/yo
PartOf=web.target

[Mount]
What=/dev/sdb1
Where=/opt/yo
Type=ext4
Options=defaults
```

```
$ systemctl cat opt-yo.automount 
# /etc/systemd/system/opt-yo.automount
[Unit]
Description=Automount /opt/yo
ConditionPathExists=/opt/yo
PartOf=web.target

[Automount]
Where=/opt/yo
TimeoutIdleSec=10

[Install]
WantedBy=web.target
```

Test :
```
$ sudo systemctl is-active {web.target,web.service,opt-yo.mount,opt-yo.automount}
inactive
inactive
inactive
inactive

$ sudo systemctl start web.target

$ sudo systemctl is-active {web.target,web.service,opt-yo.mount,opt-yo.automount}
active
active
active
active

$ sudo systemctl stop web.target

$ sudo systemctl is-active {web.target,web.service,opt-yo.mount,opt-yo.automount}
inactive
inactive
inactive
inactive
```

---

End :o

```
$ sudo systemctl isolate shutdown.target
```
